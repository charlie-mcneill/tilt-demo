﻿using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.A)) {
			transform.Rotate(0,0,1);
		}

		if (Input.GetKey (KeyCode.D)) {
			transform.Rotate(0,0,-1);
		}

		if (Input.GetKey (KeyCode.S)) {
			transform.Rotate(-1,0,0);
		}

		if (Input.GetKey (KeyCode.W)) {
			transform.Rotate(1,0,0);
		}
	
	}
}
